package main

import "fmt"

func printArray(arr []int) {
	arr[0] = 100
	for i, v := range arr {
		fmt.Println(i, v)
	}

}

func main() {
	fmt.Println("array test")

	var arr1 [5]int
	arr2 := [3]int{1, 3, 5}
	arr3 := [...]int{2, 4, 5, 6, 8}

	fmt.Println(arr1, arr2, arr3)

	var grid [4][5]int
	fmt.Println(grid)

	//for i := 0; i < len(arr3) ; i++  {
	//	fmt.Println(arr3[i])
	//}

	fmt.Println("printarray(arr1)")
	printArray(arr1[:])
	fmt.Println("printarray(arr1)")
	fmt.Println(arr1)

}
