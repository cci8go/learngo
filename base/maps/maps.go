package main

import "fmt"

func main() {
	m := map[string]string{
		"1": "a",
		"2": "b",
	}

	fmt.Println(len(m))
	m2 := make(map[string]int)

	var m3 map[string]int

	fmt.Println(m, m2, m3)

	for k, v := range m {
		fmt.Println(k, v)
	}

	a, ok := m["1"]
	fmt.Println(a, ok)
	c, ok := m["c"]
	fmt.Println(c, ok)

	if a, ok := m["1a"]; ok {
		fmt.Println(a)
	} else {
		fmt.Println("key does not exist")
	}

	fmt.Println("deleteing map values")
	delete(m, "1")
	fmt.Println(m)

	//map 是无序的

	fmt.Println(len(m))
}
