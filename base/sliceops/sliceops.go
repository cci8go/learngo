package main

import "fmt"

func printslice(s []int) {
	fmt.Printf("len=%d cap=%d \n", len(s), cap(s))
}

func main() {
	var s []int // Zero value for slice is nil
	fmt.Println(s)

	for i := 0; i < 100; i++ {
		s = append(s, 2*i+1)
		printslice(s)
	}

	fmt.Println(s)

}
