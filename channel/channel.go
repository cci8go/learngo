package main

import (
	"fmt"
	"sync"
)

func doWork(id int, c chan int, w worker) {
	for n := range c {
		fmt.Printf("Worker %d received %c\n", id, n)
		w.done()
	}
}

type worker struct {
	in   chan int
	done func()
}

func createWorker(id int, wg *sync.WaitGroup) worker {
	w := worker{
		in: make(chan int),
		done: func() {
			wg.Done()
		},
	}
	go doWork(id, w.in, w)
	return w
}

func chanDemo() {

	//只能发 chan<-
	//只能收 <-chan

	var wg sync.WaitGroup

	var workers [10]worker
	for i := 0; i < 10; i++ {
		workers[i] = createWorker(i, &wg)
	}
	wg.Add(20)

	for i, worker := range workers {
		worker.in <- 'a' + i

	}

	for i, worker := range workers {
		worker.in <- 'A' + i

	}

	wg.Wait()
}

//func bufferedChannel() {
//	c := make(chan int, 3)
//	go doWork(0, c)
//	c <- 'a'
//	c <- 'b'
//	c <- 'c'
//	c <- 'd'
//	time.Sleep(time.Millisecond)
//}

//func channelClose() {
//	c := make(chan int, 3)
//	go doWork(0, c)
//	c <- 'a'
//	c <- 'b'
//	c <- 'c'
//	c <- 'd'
//	close(c)
//	time.Sleep(time.Millisecond)
//}

func main() {

	chanDemo()
}
