package main

import "fmt"

var lastOccurred = make([]int, 0xffff)

func lengthOfNonRepeatingSubStr(s string) int {

	//lastOccurred := make(map[rune]int)
	//lastOccurred := make([]int, 0xffff)

	for i := range lastOccurred {
		lastOccurred[i] = -1
	}

	start := 0
	maxLength := 0
	for i, ch := range []rune(s) {

		if lastI := lastOccurred[ch]; lastI != -1 && lastI >= start {
			start = lastI + 1
		}

		if i-start+1 > maxLength {
			maxLength = i - start + 1
		}

		lastOccurred[ch] = i
	}

	//fmt.Println(lastOccurred)
	return maxLength
}

func twoSum(nums []int, target int) []int {

	var m = make(map[int]int)
	for i := 0; i < len(nums); i++ {
		complement := target - nums[i]
		if value, ok := m[complement]; ok {
			return []int{value, i}
		}
		m[nums[i]] = i

	}
	return nil
}

func main() {

	fmt.Println(lengthOfNonRepeatingSubStr("abcdaaaadfd"))
	fmt.Println(lengthOfNonRepeatingSubStr("这aaahah哈哈"))

	//arr := [] int {1,3,5,6}
	//var result = twoSum(arr,7)
	//fmt.Println(result)
}
