package main

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

func main() {
	s := "yes我爱佳佳呀!"
	fmt.Printf("%X", s)

	fmt.Println()
	for i, ch := range s {
		fmt.Printf("(%d %X) ", i, ch)
	}
	fmt.Println()
	fmt.Println(utf8.RuneCountInString(s))

	bytes := []byte(s)
	for len(bytes) > 0 {
		ch, size := utf8.DecodeRune(bytes)
		bytes = bytes[size:]
		fmt.Printf("%c ", ch)
	}
	fmt.Println()

	for i, ch := range []rune(s) {
		fmt.Printf("(%d %d) ", i, ch)
	}

	fmt.Println()
	result := strings.ToLower("wwwA")
	fmt.Println(result)

}
