package view

import (
	"os"
	"testing"

	"learngo/crawler/engine"
	"learngo/crawler/frontend/model"
	common "learngo/crawler/model"
)

func TestSearchResultView_Render(t *testing.T) {
	view := CreateSearchResultView(
		"template.html")

	out, err := os.Create("template.test.html")
	if err != nil {
		panic(err)
	}
	defer out.Close()

	page := model.SearchResult{}
	page.Hits = 123
	item := engine.Item{
		Url:  "http://album.zhenai.com/u/1102943752",
		Type: "zhenai",
		Id:   "1102943752",
		Payload: common.Profile{
			Name:      "好好",
			Marriage:  "未婚",
			Education: "大专",
			Age:       "26岁",
			//Income:    "3001-5000元",
			Height: "168cm",
		},
	}
	for i := 0; i < 10; i++ {
		page.Items = append(page.Items, item)
	}

	err = view.Render(out, page)
	if err != nil {
		t.Error(err)
	}

	// TODO: verify contents in template.test.html
}
