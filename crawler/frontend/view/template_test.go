package view

import (
	"html/template"
	"learngo/crawler/engine"
	"learngo/crawler/frontend/model"
	common "learngo/crawler/model"
	"os"
	"testing"
)

func TestTemplate(t *testing.T) {
	template := template.Must(template.ParseFiles("template.html"))

	out, err := os.Create("template.test.html")

	page := model.SearchResult{}
	page.Hits = 10
	item := engine.Item{
		Url:  "http://album.zhenai.com/u/1102943752",
		Type: "zhenai",
		Id:   "1102943752",
		Payload: common.Profile{
			Name:      "好好",
			Marriage:  "未婚",
			Education: "大专",
			Age:       "26岁",
			Hukou:     "上海",
			Income:    "3001-5000元",
			Height:    "168cm",
		},
	}

	for i := 0; i < 5; i++ {
		page.Items = append(page.Items, item)
	}

	err = template.Execute(out, page)
	if err != nil {
		panic(err)
	}

}
