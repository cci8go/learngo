package main

// 下载这个包 gopm get -g -v golang.org/x/text 转化编码
// gopm get -g -v golang.org/x/net/html

import (
	"learngo/crawler/engine"
	"learngo/crawler/zhenai/parser"
)

func main() {

	//单任务
	simpleEngine()

	//itemChan, err := persist.ItemSaver("dating_profile")
	//if err != nil {
	//	panic(err)
	//}
	//
	//e := engine.ConcurrentEngine{
	//	Scheduler:   &scheduler.QueuedScheduler{},
	//	WorkerCount: 100,
	//	ItemChan:    itemChan,
	//}
	//
	//e.Run(engine.Request{
	//	Url:        "http://www.zhenai.com/zhenghun",
	//	ParserFunc: parser.ParseCityList,
	//})

}

func simpleEngine() {
	e := engine.SimpleEngine{}

	e.Run(engine.Request{
		Url:        "http://www.zhenai.com/zhenghun",
		ParserFunc: parser.ParseCityList,
	})

}
