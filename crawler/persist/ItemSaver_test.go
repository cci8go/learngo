package persist

import (
	"context"
	"encoding/json"
	"learngo/crawler/engine"
	"learngo/crawler/model"
	"testing"

	elastic "gopkg.in/olivere/elastic.v5"
)

func TestSaver(t *testing.T) {

	expected := engine.Item{
		Url:  "http://album.zhenai.com/u/1102943752",
		Type: "zhenai",
		Id:   "1102943752",
		Payload: model.Profile{
			Name:      "好好",
			Marriage:  "未婚",
			Education: "大专",
			Age:       "26岁",
			Income:    "3001-5000元",
			Height:    "168cm",
		},
	}

	client, err := elastic.NewClient(
		elastic.SetSniff(false))
	if err != nil {
		panic(err)
	}

	//Sav expected item
	const index = "dating_test"
	err = save(client, index, expected)
	if err != nil {
		panic(err)
	}

	// TODO :Tyy to start up elastic search
	// here using docker go client

	//Fetch saved item
	resp, err := client.Get().
		Index(index).
		Type(expected.Type).
		Id(expected.Id).
		Do(context.Background())

	if err != nil {
		panic(err)
	}

	t.Logf("%s", *resp.Source)
	var actual engine.Item
	err = json.Unmarshal(
		[]byte(*resp.Source), &actual)
	if err != nil {
		panic(err)
	}

	//Verify result
	actualPrifile, _ := model.FromJsonObj(actual.Payload)
	actual.Payload = actualPrifile

	if actual != expected {
		t.Errorf("got %v; expected %v", actual, expected)
	}
}
