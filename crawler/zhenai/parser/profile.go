package parser

import (
	"learngo/crawler/engine"
	"learngo/crawler/model"
	"regexp"
	"strings"
)

var ageRe = regexp.MustCompile(``)
var marriageRe = regexp.MustCompile(``)

//<div class="des f-cl" data-v-3c42fade>遵义 | 32岁 | 中专 | 离异 | 155cm | 5001-8000元</div>
//<div class="des f-cl" data-v-3c42fade="">阿坝 | 48岁 | 大专 | 离异 | 173cm | 3001-5000元</div>

//阿坝 | 37岁 | 大学本科 | 离异 | 160cm | 3001-5000元
//var profileRe = regexp.MustCompile(`<div class="des f-cl" data-v-3c42fade>(.*?)[|](.*?)[|](.*?)[|](.*?)[|](.*?)[|](.*?)\/div>`)

var (
	profileRe = regexp.MustCompile(`<div class="des f-cl" data-v-3c42fade>(.*?)</div>`)
	idUrlRe   = regexp.MustCompile(`http://album.zhenai.com/u/([\d]+)`)
)

func ParseProfile(contents []byte, url string, name string) engine.ParseResult {

	profile := model.Profile{}
	profile.Name = name

	match := profileRe.FindSubmatch(contents)
	//fmt.Printf("contents: %s\n", contents)
	//for _, m := range match {
	//	fmt.Printf("match: %s\n", m)
	//}

	if len(match) >= 2 {

		matchValue := strings.Split(string(match[1]), "|")
		//fmt.Printf("matchValue: %s\n", matchValue)

		profile.Hukou = strings.TrimSpace(string(matchValue[0]))
		profile.Age = strings.TrimSpace(string(matchValue[1]))
		profile.Education = strings.TrimSpace(string(matchValue[2]))
		profile.Marriage = strings.TrimSpace(string(matchValue[3]))
		profile.Height = strings.TrimSpace(string(matchValue[4]))
		//profile.Income = strings.TrimSpace(string(matchValue[5]))
	}

	result := engine.ParseResult{
		Items: []engine.Item{
			{
				Url:     url,
				Type:    "zhenai",
				Id:      extractString([]byte(url), idUrlRe),
				Payload: profile,
			},
		},
	}

	return result
}

func extractString(contents []byte, re *regexp.Regexp) string {
	match := re.FindSubmatch(contents)
	if len(match) >= 2 {
		return string(match[1])
	} else {
		return ""
	}

}

func ProfileParser(name string) engine.ParserFunc {
	return func(c []byte, url string) engine.ParseResult {
		return ParseProfile(c, url, name)
	}
}
