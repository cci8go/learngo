package parser

import (
	"fmt"
	"io/ioutil"
	"learngo/crawler/engine"
	"learngo/crawler/model"
	"testing"
)

//<div class="des f-cl" data-v-3c42fade>遵义 | 32岁 | 中专 | 离异 | 155cm | 5001-8000元</div>
func TestParseProfile(t *testing.T) {
	contents, err := ioutil.ReadFile("profile_test_data.html")
	if err != nil {
		panic(err)
	}

	fmt.Printf("%s\n", contents)

	result := ParseProfile(contents,
		"http://album.zhenai.com/u/1102943752", "好好")

	if len(result.Items) != 1 {
		t.Errorf("Result should contail 1 "+"element; but was %v", result.Items)
	}

	actual := result.Items[0]

	expected := engine.Item{
		Url:  "http://album.zhenai.com/u/1102943752",
		Type: "zhenai",
		Id:   "1102943752",
		Payload: model.Profile{
			Name:      "好好",
			Marriage:  "未婚",
			Education: "大专",
			Age:       "26岁",
			//Income:    "3001-5000元",
			Height: "168cm",
		},
	}

	if actual != expected {
		t.Errorf("expected %v; but was %v ", expected, actual)
	}
}
