package main

import (
	"fmt"
	"os"
)

/**
6 5
0 1 0 0 0
0 0 0 1 0
0 1 0 1 0
1 1 1 0 0
0 1 0 0 1
0 1 0 0 0
**/

func readMaze(fileName string) [][]int {
	file, err := os.Open(fileName)
	if err != nil {
		panic(err)
	}
	var row, col int
	fmt.Fscanf(file, "%d %d", &row, &col)
	//行
	maze := make([][]int, row)
	for i := range maze {
		//列
		maze[i] = make([]int, col)
		for j := range maze[i] {
			fmt.Fscanf(file, "%d", &maze[i][j])
		}
	}
	return maze
}

type point struct {
	i, j int
}

var dirs = [4]point{
	//上、左、下、右
	{-1, 0}, {0, -1}, {1, 0}, {0, 1},
}

func (p point) add(r point) point {
	return point{p.i + r.i, p.j + r.j}
}

func (p point) at(grid [][]int) (int, bool) {
	//越界了
	if p.i < 0 || p.i >= len(grid) {
		return 0, false
	}

	//
	if p.j < 0 || p.j >= len(grid[p.i]) {
		return 0, false
	}

	return grid[p.i][p.j], true
}

func walk(maze [][]int, start, end point) [][]int {
	steps := make([][]int, len(maze))

	for i := range steps {
		steps[i] = make([]int, len(maze[i]))
	}

	fmt.Println("steps=", steps)

	Q := []point{start}
	fmt.Println("Q1=", Q)
	for len(Q) > 0 {
		cur := Q[0]
		Q = Q[1:]
		fmt.Println("Q2=", Q)
		if cur == end {
			break
		}

		for _, dir := range dirs {
			next := cur.add(dir)

			//maze at next is 0
			//and steps at next is 0
			//and next != start

			val, ok := next.at(maze)
			if !ok || val == 1 {
				//不能看这个点
				continue
			}

			val, ok = next.at(steps)
			if !ok || val != 0 {
				//不能看这个点
				continue
			}

			if next == start {
				continue
			}

			curSteps, _ := cur.at(steps)
			fmt.Println("curSteps=", curSteps)

			steps[next.i][next.j] = curSteps + 1
			Q = append(Q, next)
			fmt.Println("Q3=", Q)
		}
	}
	return steps
}

func main() {
	maze := readMaze("maze/maze.in")

	for _, row := range maze {
		for _, val := range row {
			fmt.Printf("%d ", val)
		}
		fmt.Println()
	}
	steps := walk(maze, point{0, 0}, point{len(maze) - 1, len(maze[0]) - 1})

	for _, row := range steps {
		for _, val := range row {
			fmt.Printf("%3d ", val)
		}
		fmt.Println()
	}
}
