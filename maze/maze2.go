package main

import (
	"fmt"
	"os"
)

func readMazeFile(fileName string) [][]int {
	file, err := os.Open(fileName)
	if err != nil {
		panic(err)
	}

	var row, col int
	fmt.Fscanf(file, "%d %d", &row, &col)
	fmt.Println("row:", row, "col:", col)
	maze := make([][]int, row)

	for i := range maze {
		maze[i] = make([]int, col)
		for j := range maze[i] {
			fmt.Fscanf(file, "%d", &maze[i][j])
		}
	}
	return maze
}

type pointNew struct {
	i, j int
}

var dirsNew = []pointNew{
	//上 左 下 右
	{-1, 0}, {0, -1}, {1, 0}, {0, 1},
}

func (p pointNew) addNew(r pointNew) pointNew {
	return pointNew{p.i + r.i, p.j + r.j}
}

func (p pointNew) atNew(grid [][]int) (int, bool) {

	//越界
	if p.i < 0 || p.i >= len(grid) {
		return 0, false
	}

	//越界
	if p.j < 0 || p.j >= len(grid[p.i]) {
		return 0, false
	}

	return grid[p.i][p.j], true
}

func walkStep(maze [][]int, start pointNew, end pointNew) [][]int {
	step := make([][]int, len(maze))
	for i := range step {
		step[i] = make([]int, len(maze[0]))
	}

	Q := []pointNew{start}
	fmt.Println("Q1:", Q)

	for len(Q) > 0 {
		cur := Q[0]
		Q = Q[1:]
		fmt.Println("Q2:", Q)

		if cur == end {
			break
		}

		for _, dir := range dirsNew {

			next := cur.addNew(dir)
			fmt.Println("cur:", cur, "dir:", dir, "next:", next)

			val, ok := next.atNew(maze)
			if !ok || val == 1 {
				continue
			}

			val, ok = next.atNew(step)
			if !ok || val != 0 {
				//不能看这个点
				continue
			}

			if next == start {
				continue
			}

			curSteps, _ := cur.atNew(step)
			fmt.Println("curSteps=", curSteps)
			step[next.i][next.j] = curSteps + 1
			Q = append(Q, next)
			fmt.Println("Q3=", Q)
		}

	}

	return step
}

func main() {
	//从文件读数据
	maze := readMazeFile("maze/maze.in")
	for _, row := range maze {
		for _, col := range row {
			fmt.Printf("%d ", col)
		}
		fmt.Println()
	}

	//
	fmt.Println("开始寻找路径\n")

	step := walkStep(maze, pointNew{0, 0}, pointNew{len(maze) - 1, len(maze[0]) - 1})
	for _, row := range step {
		for _, col := range row {
			fmt.Printf("%3d ", col)
		}
		fmt.Println()
	}
}
