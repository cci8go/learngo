package main

import (
	"fmt"
	"learngo/queue"
)

func main() {
	q := queue.Queue{1}
	fmt.Println(q)
	q.Push(2)
	q.Push("abc")
	fmt.Println(q)
	q.Push(3)
	fmt.Println(q)

	fmt.Println(q.Pop())
	fmt.Println(q)
	fmt.Println(q.Pop())
	fmt.Println(q)
	fmt.Println(q.IsEmpty())
	fmt.Println(q.Pop())
	fmt.Println(q)
	fmt.Println(q.IsEmpty())

}
