package main

import (
	"fmt"
	"regexp"
)

const text = `
my email is 153190953@qq.com@abc.com
email is abc@163.org
cci8go@163.com
email3 is ddd@gmail.com.cn 
`

func main() {
	re := regexp.MustCompile(`([a-zA-Z0-9]+)@([a-zA-Z0-9]+)(\.[a-zA-Z0-9.]+)`)
	match := re.FindAllStringSubmatch(text, -1)

	for _, m := range match {
		fmt.Println(m)
	}
}
