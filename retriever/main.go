package main

import (
	"fmt"
	"learngo/retriever/mock"
	"learngo/retriever/real"
	"time"
)

type Retriever interface {
	Get(url string) string
}

func download(r Retriever) string {
	return r.Get("http://www.imooc.com")
}

func main() {
	var r Retriever
	r = &mock.Retriever{"This is fake mock com"}

	//fmt.Printf("%T %v\n", r, r)
	//fmt.Println(download(r))
	inspect(r)

	r = &real.Retriever{
		UserAgent: "5.0",
		TimeOut:   time.Minute,
	}
	//fmt.Printf("%T %v\n", r, r)
	//fmt.Println(download(r))

	inspect(r)

	//Type assrtion
	if realRetriever, ok := r.(*real.Retriever); ok {
		fmt.Println(realRetriever.TimeOut)
	} else {
		fmt.Println("not a real retriever")
	}

}

func inspect(r Retriever) {
	switch v := r.(type) {
	case mock.Retriever:
		fmt.Println("Contents: ", v.Contents)
	case *real.Retriever:
		fmt.Println("UserAgent: ", v.UserAgent)
	}
}
